# docker configuration
source .venv/bin/activate
sudo service docker start

# ping all or specific servers depending on inventory
ansible all -i inventories/dev -u root -m ping
ansible web -i inventories/dev -u root -m ping

# launch role with tags
ansible-playbook docker.yml -u besstial -v --tags deploy
ansible-playbook -i inventories/dev nginx.yml -u root --skip-tags uninstall -v
ansible-playbook -i inventories/dev nginx.yml -u root --skip-tags install -v
ansible-playbook docker.yml -u besstial -v --tags kill

# test 
curl http://172.17.0.3
and go to http://localhost:83













# Docker commands helper
## Arrêter tous les conteneurs en cours d'exécution
sudo docker stop $(sudo docker ps -q)

## Supprimer tous les conteneurs
sudo docker rm $(sudo docker ps -a -q)

## Supprimer toutes les images (avec confirmation)
sudo docker rmi -f $(sudo docker images -q)

sudo docker build -t node .
for i in {2..5}; do sudo docker run -d --name "node$i" node; done
sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(sudo docker ps -q --filter "name=node")

# ssh configuration
for i in {2..5}; do ssh-keygen -f "/home/besstial/.ssh/known_hosts" -R "172.17.0.$i"; done
ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -N "" -y
for i in {2..5}; do sshpass -p ansible ssh-copy-id -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa root@172.17.0.$i; done


docker container ls
docker exec -it b31543b9a420 /bin/bash
